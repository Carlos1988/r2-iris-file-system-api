package org.r2.iris.file.system.app.propertyeditor;
		
import java.beans.PropertyEditorSupport;
		import java.util.Date;
		import java.util.regex.Pattern;
		
		import org.apache.commons.lang3.StringUtils;
		
/**
 * @author Allan G. Ramirez (aramirez@lingotek.com)
 */
public class DatePropertyEditorSupport extends PropertyEditorSupport {
    private static final Pattern NUMBER_PATTERN = Pattern.compile("^[0-9]+$");

    public String getJavaInitializationString() {
        Object var1 = this.getValue();
        return var1 != null ? var1.toString() : "null";
    }

    public void setAsText(String value) {
        Object result = null;
        if (StringUtils.isNotBlank(value) && NUMBER_PATTERN.matcher(value.trim()).matches()) {
            result = new Date(Long.parseLong(value.trim()));
        }
        setValue(result);

    }

    public String getAsText() {
        Object var1 = this.getValue();
        return var1 instanceof Date ? Long.toString(((Date) var1).getTime()) : null;
    }
}
