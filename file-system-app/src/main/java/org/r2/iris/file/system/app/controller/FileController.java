package org.r2.iris.file.system.app.controller;


import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Optional;
import java.util.function.Function;

import org.r2.iris.file.system.app.exception.NotFoundException;
import org.r2.iris.file.system.service.FileService;
import org.r2.iris.file.system.service.common.dto.FileDto;
import org.r2.iris.file.system.service.common.dto.IdDto;
import org.r2.iris.file.system.service.exception.FileStoreException;
import org.r2.iris.file.system.service.exception.GenericServiceException;
import org.r2.iris.file.system.service.model.FileIndex;
import org.r2.iris.file.system.service.repo.FileIndexRepo;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/files")
public class FileController {
	private final long _16MB = 16000000;
 	
	@Autowired
	private BeanFactory factory;
 	
	@Autowired
	private FileIndexRepo fileIndexRepo;
	
 	private final Function<Boolean, FileService> fileServiceBean = 
 			(hasExceed16MB) ->  
 				hasExceed16MB ? 
 					factory.getBean("gridfs", FileService.class) :
 					factory.getBean("basic", FileService.class); 
	
	@PostMapping
	public @ResponseBody IdDto save(@RequestParam("file") MultipartFile file) throws FileStoreException {
		return  fileServiceBean.apply(_16MB <= file.getSize()).save(file);
	}
	
//	@PostMapping("/bulk")
//	public @ResponseBody List<IdDto> save(@RequestParam("file") MultipartFile[] files) throws FileStoreException {
//		List<MultipartFile> largeFiles = Arrays.asList(files).stream().filter((file) -> _16MB <= file.getSize()).collect(Collectors.toList());
//		List<MultipartFile> smallFiles = Arrays.asList(files).stream().filter((file) -> _16MB > file.getSize()).collect(Collectors.toList());;
//		List<IdDto> savedFiles = new ArrayList<>();
//		try {
//			savedFiles = fileServiceBean.apply(true).save(largeFiles); // 1st bulk transaction
//			savedFiles.addAll(fileServiceBean.apply(false).save(smallFiles)); // 2nd bulk Transaction
//			return savedFiles;
//		} catch(FileStoreException e) {
//			if (!savedFiles.isEmpty()) {
//				return savedFiles;
//			} else throw e;
//		}
//	}
	
	@GetMapping("/{id}/content")
	public HttpEntity<byte[]> get(@PathVariable("id") String id) throws FileStoreException, GenericServiceException {
		try {
			Optional<FileIndex> fileIndex = Optional.ofNullable(fileIndexRepo.findOne(id));
			if(fileIndex.isPresent()) {
				FileDto file = fileServiceBean.apply(fileIndex.get().isHasExceed16MbSize()).retrieveById(fileIndex.get().getId());
				ByteArrayOutputStream os = new ByteArrayOutputStream();
		        os.write((byte[])file.getData());
		        HttpHeaders headers = new HttpHeaders();
		        headers.add(HttpHeaders.CONTENT_TYPE, (String)file.getContentType());
		        return new HttpEntity<>(os.toByteArray(), headers);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND); 
			}
		} catch (IOException e) {
			throw new FileStoreException(e.getMessage());
		}
	}
	
	@PostMapping("/{id}")
	public @ResponseBody void update(@PathVariable("id") String id,  @RequestParam("file") MultipartFile file) {
		final Optional<FileIndex> maybefileIndex = Optional.ofNullable(fileIndexRepo.findOne(id));
		
		if(!maybefileIndex.isPresent()) 
			throw new NotFoundException("File with ".concat(id).concat(" not found."));
		
		final FileIndex fileIndex = maybefileIndex.get();
		
		//If the current file is less than 16 MB but new update has greater size or vice versa.
		if((fileIndex.isHasExceed16MbSize() != _16MB <= file.getSize())){
			final FileService fileServiceBaseOnRepo = 
					fileServiceBean.apply(fileIndex.isHasExceed16MbSize());
			fileServiceBaseOnRepo.delete(id);
		}
		
		final FileService fileService = fileServiceBean.apply( _16MB <= file.getSize());
		fileService.update(id, file);
	}
	
	@DeleteMapping("/{id}")
	public @ResponseBody void delete(@PathVariable("id") String id) {
		final Optional<FileIndex> maybefileIndex = Optional.ofNullable(fileIndexRepo.findOne(id));
		
		if(!maybefileIndex.isPresent()) 
			throw new NotFoundException("File with ".concat(id).concat(" not found."));
		
		final FileIndex fileIndex = maybefileIndex.get();
		
		final FileService fileService = fileServiceBean.apply(fileIndex.isHasExceed16MbSize());
		fileService.delete(id);
	}
}

