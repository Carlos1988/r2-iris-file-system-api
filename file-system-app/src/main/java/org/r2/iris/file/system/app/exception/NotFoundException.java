package org.r2.iris.file.system.app.exception;

public class NotFoundException extends RuntimeException {
    /**
	 * 
	 */
	private static final long serialVersionUID = 5454022725241667600L;

	public NotFoundException(String message) {
        super(message);
    }
}
