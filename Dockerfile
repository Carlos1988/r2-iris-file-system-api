FROM frolvlad/alpine-oraclejdk8:slim
ENV APP /usr/app
ENV SRC /usr/src
RUN mkdir -p $APP && mkdir -p $SRC

RUN wget http://mirror.rise.ph/apache/maven/maven-3/3.3.9/binaries/apache-maven-3.3.9-bin.zip -P $APP
RUN unzip $APP/apache-maven-3.3.9-bin.zip -d $APP
ENV PATH="${APP}/apache-maven-3.3.9/bin:${PATH}"
RUN echo $PATH

COPY . $SRC
WORKDIR $SRC
RUN mvn clean install

VOLUME /tmp

ENV JAVA_OPTS=""
ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar $SRC/file-system-app/target/r2-iris-file-system-api.jar" ]