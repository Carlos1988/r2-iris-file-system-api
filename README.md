# R2 IRIS API #

**R2 Integrated Research and Information System File System API** is a private service for all police intelligence images and files.

## **For developers of this service**

### Prerequisites ###

* Install [Java 8 or higher](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
* Install [Maven 3](https://maven.apache.org/download.cgi)
* Install [Mongo 3.4.3](https://www.mongodb.com/download-center#community)
* Create 'iris-file-storage' database in mongo

### Building and Running the application ###
* clone this repo
* Build the project
```
cd r2-iris-file-system-api
mvn clean install
```
* Run the server:
```
cd file-system-app
mvn spring-boot:run
```
