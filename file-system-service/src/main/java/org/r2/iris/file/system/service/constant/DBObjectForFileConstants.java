package org.r2.iris.file.system.service.constant;

public final class DBObjectForFileConstants {
	public static final String LENGTH_KEY = "length";
	public static final String UPLOAD_DATE_KEY = "uploadDate";
	public static final String FILE_NAME_KEY = "filename";
	public static final String CONTENT_TYPE_KEY = "contentType";
	public static final String DATA_KEY = "data";
	public static final String METADATA_KEY = "metadata";
	public static final String FILE_INDEX_ID_METADATA_KEY = "file_index_id";
	
	public static final String LESS_16MB_FILE_COLLECTION_NAME = "smallfile";
}
