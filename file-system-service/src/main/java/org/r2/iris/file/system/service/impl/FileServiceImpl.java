package org.r2.iris.file.system.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.r2.iris.file.system.service.FileService;
import org.r2.iris.file.system.service.common.dto.FileDto;
import org.r2.iris.file.system.service.common.dto.IdDto;
import org.r2.iris.file.system.service.constant.DBObjectForFileConstants;
import org.r2.iris.file.system.service.exception.FileStoreException;
import org.r2.iris.file.system.service.exception.GenericServiceException;
import org.r2.iris.file.system.service.model.FileIndex;
import org.r2.iris.file.system.service.repo.FileIndexRepo;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

@Service("basic")
public class FileServiceImpl implements FileService{
	private final FileIndexRepo fileIndexRepo;
	private final MongoTemplate mongoTemplate;
	
	final Function<String, Query> findByMetadataOfFileIndexIdQuery = 
		(file_index_id) -> {
			Criteria metadataCrieria = new Criteria(DBObjectForFileConstants.METADATA_KEY + "." + DBObjectForFileConstants.FILE_INDEX_ID_METADATA_KEY);
			return Query.query(metadataCrieria.is(file_index_id));
		};
	
	private Optional<DBObject> findOne(String file_index_id) {
		final DBObject dbFile = 
				this.mongoTemplate
				.findOne(findByMetadataOfFileIndexIdQuery
						.apply(file_index_id),
							BasicDBObject.class,
							DBObjectForFileConstants.LESS_16MB_FILE_COLLECTION_NAME);
		return Optional.ofNullable(dbFile);
	}
		
	public FileServiceImpl(FileIndexRepo fileIndexRepo, MongoTemplate template) {
		this.fileIndexRepo = fileIndexRepo;
		this.mongoTemplate = template;
	}
	
	private DBObject convertMultipartToBson(MultipartFile file) throws IOException {
		final DBObject dbObject = new BasicDBObject();
		dbObject.put(DBObjectForFileConstants.LENGTH_KEY, file.getSize());
		dbObject.put(DBObjectForFileConstants.UPLOAD_DATE_KEY, new Date());
		dbObject.put(DBObjectForFileConstants.FILE_NAME_KEY, file.getOriginalFilename());
		dbObject.put(DBObjectForFileConstants.CONTENT_TYPE_KEY, file.getContentType());
		InputStream f = file.getInputStream();
		byte b[] = new byte[f.available()];
        f.read(b);
        dbObject.put(DBObjectForFileConstants.DATA_KEY, b);
		return dbObject;
	};
	
	@Override
	@Transactional
	public IdDto save(MultipartFile file) throws FileStoreException, GenericServiceException {
		try {
			final DBObject metadata = new BasicDBObject();
			final FileIndex  fileIndex = fileIndexRepo.save(new FileIndex(false));
			metadata.put(DBObjectForFileConstants.FILE_INDEX_ID_METADATA_KEY, fileIndex.getId());
			DBObject fileToSave;
			fileToSave = convertMultipartToBson(file);
			fileToSave.put(DBObjectForFileConstants.METADATA_KEY, metadata);
	        mongoTemplate.save(fileToSave, DBObjectForFileConstants.LESS_16MB_FILE_COLLECTION_NAME);
			return new IdDto(fileIndex.getId());
		} catch (IOException e) {
			throw new FileStoreException(e.getMessage());
		} catch (Exception e) {
			throw new GenericServiceException(e.getMessage());
		}
	}
	
	@Override
	@Transactional
	public List<IdDto> save(List<MultipartFile> files) throws FileStoreException, GenericServiceException {
		return files.stream().map((file) -> save(file)).collect(Collectors.toList());
	}

	@Override
	@Transactional
	public void update(String file_index_id, MultipartFile file) throws FileStoreException, GenericServiceException {
		try {
			final Optional<DBObject> maybeTarget = findOne(file_index_id);
			DBObject fileToSave;
			fileToSave = convertMultipartToBson(file);
			if (maybeTarget.isPresent()) {
				fileToSave.put("_id", maybeTarget.get().get("_id"));
				fileToSave.put(DBObjectForFileConstants.METADATA_KEY,
						maybeTarget.get().get(DBObjectForFileConstants.METADATA_KEY));
			} else {
				final DBObject metadata = new BasicDBObject();
				metadata.put(DBObjectForFileConstants.FILE_INDEX_ID_METADATA_KEY, file_index_id);
				fileToSave.put(DBObjectForFileConstants.METADATA_KEY, metadata);
			}
	        mongoTemplate.save(fileToSave, DBObjectForFileConstants.LESS_16MB_FILE_COLLECTION_NAME);
	        FileIndex fileIndex = new FileIndex(false);
	        fileIndex.setId(file_index_id);
	        fileIndexRepo.save(fileIndex);
		} catch (IOException e) {
			throw new FileStoreException(e.getMessage());
		} catch (Exception e) {
			throw new GenericServiceException(e.getMessage());
		}
	}

	@Override
	public FileDto retrieveById(String file_index_id) throws FileStoreException, GenericServiceException {
		try {
			final Optional<DBObject> maybeDbFile = findOne(file_index_id);
			if(maybeDbFile.isPresent()){
				FileDto filedto = new FileDto();
				filedto.setContentType(String.valueOf(maybeDbFile.get()
						.get(DBObjectForFileConstants.CONTENT_TYPE_KEY)));
				filedto.setData((byte[])maybeDbFile.get()
						.get(DBObjectForFileConstants.DATA_KEY));
				filedto.setFileName(String.valueOf(maybeDbFile.get()
						.get(DBObjectForFileConstants.FILE_NAME_KEY)));
				filedto.setId(String.valueOf(((DBObject)maybeDbFile.get()
						.get(DBObjectForFileConstants.METADATA_KEY))
						.get(DBObjectForFileConstants.FILE_INDEX_ID_METADATA_KEY)));
				filedto.setLength(Long.valueOf(String.valueOf(maybeDbFile.get()
						.get(DBObjectForFileConstants.LENGTH_KEY))));
				filedto.setUploadDate((Date)maybeDbFile.get().get(DBObjectForFileConstants.UPLOAD_DATE_KEY));
				return filedto;
			}
		} catch (Exception e) {
			throw new GenericServiceException(e.getMessage());
		}
	    return null;
	}

	@Override
	@Transactional
	public void delete(String file_index_id) {
		fileIndexRepo.delete(file_index_id);
		mongoTemplate.remove(findByMetadataOfFileIndexIdQuery.apply(file_index_id),
				DBObjectForFileConstants.LESS_16MB_FILE_COLLECTION_NAME);
	}
}