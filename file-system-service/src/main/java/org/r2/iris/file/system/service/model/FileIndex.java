package org.r2.iris.file.system.service.model;

import org.springframework.data.annotation.Id;

public class FileIndex {
	@Id
	String id;
	
	boolean hasExceed16MbSize;
	
	public boolean isHasExceed16MbSize() {
		return hasExceed16MbSize;
	}

	public FileIndex(boolean hasExceed16MbSize){
		this.hasExceed16MbSize = hasExceed16MbSize;
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
}
