package org.r2.iris.file.system.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.commons.io.IOUtils;
import org.r2.iris.file.system.service.FileService;
import org.r2.iris.file.system.service.common.dto.FileDto;
import org.r2.iris.file.system.service.common.dto.IdDto;
import org.r2.iris.file.system.service.constant.DBObjectForFileConstants;
import org.r2.iris.file.system.service.exception.FileStoreException;
import org.r2.iris.file.system.service.exception.GenericServiceException;
import org.r2.iris.file.system.service.model.FileIndex;
import org.r2.iris.file.system.service.repo.FileIndexRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsCriteria;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.gridfs.GridFSDBFile;

@Service("gridfs")
public class GridfsFileServiceImpl implements FileService {
	private final GridFsTemplate gridfsTemplate;
	private final FileIndexRepo fileIndexRepo;
	private GridfsFileServiceImpl self = this;
	
	final Function<String, Query> findByMetadataOfFileIndexIdQuery = 
			(file_index_id) ->
					Query.query(GridFsCriteria.whereMetaData(DBObjectForFileConstants.FILE_INDEX_ID_METADATA_KEY)
							.is(file_index_id));
	
	private Optional<GridFSDBFile> findOne(String file_index_id) {
		final GridFSDBFile fsdbFile = 
				self.gridfsTemplate
				.findOne(findByMetadataOfFileIndexIdQuery.apply(file_index_id));
		return Optional.ofNullable(fsdbFile);
	}

	@Autowired
	public GridfsFileServiceImpl(FileIndexRepo fileIndexRepo, GridFsTemplate gridfsTemplate) {
		this.fileIndexRepo = fileIndexRepo;
		this.gridfsTemplate = gridfsTemplate;
	}
	
	@Override
	@Transactional
	public List<IdDto> save(List<MultipartFile> files) throws FileStoreException, GenericServiceException {
		return  files.stream().map((file) -> save(file)).collect(Collectors.toList());
	}
	
	@Override
	@Transactional
	public IdDto save(MultipartFile file) throws FileStoreException, GenericServiceException {
		try {
			final DBObject metadata = new BasicDBObject();
			FileIndex  fileIndex = fileIndexRepo.save(new FileIndex(true));
			metadata.put(DBObjectForFileConstants.FILE_INDEX_ID_METADATA_KEY, fileIndex.getId());
			gridfsTemplate.store(file.getInputStream(), file.getOriginalFilename(),
					file.getContentType(), metadata);
			return new IdDto(fileIndex.getId());
		} catch (IOException e) {
			throw new FileStoreException(e.getMessage());
		} catch (Exception e) {
			throw new GenericServiceException(e.getMessage());
		}
	}

	@Override
	@Transactional
	public void update(String file_index_id, MultipartFile file) throws FileStoreException, GenericServiceException {
		try {
			if(findOne(file_index_id).isPresent()){
				gridfsTemplate.delete(findByMetadataOfFileIndexIdQuery.apply(file_index_id));
			}
			final DBObject metadata = new BasicDBObject();
			metadata.put(DBObjectForFileConstants.FILE_INDEX_ID_METADATA_KEY, file_index_id);
			gridfsTemplate.store(file.getInputStream(), file.getOriginalFilename(),
					file.getContentType(), metadata);
			FileIndex fileIndex = new FileIndex(true);
	        fileIndex.setId(file_index_id);
	        fileIndexRepo.save(fileIndex);
		} catch(IOException e) {
			throw new FileStoreException(e.getMessage());
		} catch (Exception e) {
			throw new GenericServiceException(e.getMessage());
		}
	}

	@Override
	public FileDto retrieveById(String file_index_id) throws FileStoreException, GenericServiceException {
		try {
			final Optional<GridFSDBFile> maybeGridFsDbFile = findOne(file_index_id);
			if(maybeGridFsDbFile.isPresent()){
				FileDto filedto = new FileDto();
				filedto.setContentType(maybeGridFsDbFile.get().getContentType());
				filedto.setData(IOUtils.toByteArray(maybeGridFsDbFile.get().getInputStream()));
				filedto.setFileName(maybeGridFsDbFile.get().getFilename());
				filedto.setId(String.valueOf(maybeGridFsDbFile.get()
						.getMetaData().get(DBObjectForFileConstants.FILE_INDEX_ID_METADATA_KEY)));
				filedto.setLength(maybeGridFsDbFile.get().getLength());
				filedto.setUploadDate((Date)maybeGridFsDbFile.get().get(DBObjectForFileConstants.UPLOAD_DATE_KEY));
				return filedto;
			}
		} catch (IOException e) {
			throw new FileStoreException(e.getMessage());
		} catch (Exception e) {
			throw new GenericServiceException(e.getMessage());
		}
		return null;
	}

	@Override
	public void delete(String file_index_id) {
		fileIndexRepo.delete(file_index_id);
		gridfsTemplate.delete(findByMetadataOfFileIndexIdQuery.apply(file_index_id));
	}
}
