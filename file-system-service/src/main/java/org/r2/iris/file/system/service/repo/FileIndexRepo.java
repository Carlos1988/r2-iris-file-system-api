package org.r2.iris.file.system.service.repo;

import org.r2.iris.file.system.service.model.FileIndex;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface FileIndexRepo extends MongoRepository<FileIndex, String>{}
