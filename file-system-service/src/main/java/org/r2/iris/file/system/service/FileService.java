package org.r2.iris.file.system.service;

import java.util.List;

import org.r2.iris.file.system.service.common.dto.FileDto;
import org.r2.iris.file.system.service.common.dto.IdDto;
import org.r2.iris.file.system.service.exception.FileStoreException;
import org.r2.iris.file.system.service.exception.GenericServiceException;
import org.springframework.web.multipart.MultipartFile;

public interface FileService {
	IdDto save(MultipartFile file) throws FileStoreException, GenericServiceException;
	List<IdDto> save(List<MultipartFile> files) throws FileStoreException, GenericServiceException;
	void update(String file_index_id, MultipartFile file) throws FileStoreException, GenericServiceException;
	FileDto retrieveById(String id) throws FileStoreException, GenericServiceException;
	void delete(String file_index_id);
}
