package org.r2.iris.file.system.service.exception;

/**
 * @author Allan G. Ramirez (aramirez@lingotek.com)
 */
public class GenericServiceException extends RuntimeException {
    public GenericServiceException(String message) {
        super(message);
    }
}
