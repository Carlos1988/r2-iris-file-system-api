package org.r2.iris.file.system.service.exception;

public class FileStoreException extends RuntimeException {
    public FileStoreException(String message) {
        super(message);
    }
}
