package org.r2.iris.file.system;

public interface FileService {
	String save();
	String findById();
}
